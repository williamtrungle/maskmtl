var iso = new Isotope('.masonry', {
  itemSelector: '.masonry-item',
  percentPosition: true,
  masonry: { horizontalOrder: true,
             gutter: '.masonry-gutter-sizer'}});

imagesLoaded(document.querySelector('.masonry')).on('progress', () => { iso.layout()});

function handleFilter() {
  fs = document.getElementById("filters").querySelectorAll(".filtered");
  filters = [];
  for (e of fs) {
    if (e.checked) {
      filters.push(e.dataset.filter);
    }
  }
  filter = filters.map(x => "." + x).join("");
  iso.arrange({filter: filter});
}

Array.prototype.forEach.call(document.querySelectorAll('[type=radio]'), function(radio) {
  radio.addEventListener('click', function(){
    var self = this;
    // get all elements with same name but itself and mark them unchecked
    Array.prototype.filter.call(document.getElementsByName(this.name), function(filterEl) {
      return self !== filterEl;
    }).forEach(function(otherEl) {
      delete otherEl.dataset.check
    })

    // set state based on previous one
    if (this.dataset.hasOwnProperty('check')) {
      this.checked = false
      delete this.dataset.check
    } else {
      this.dataset.check = ''
    }
    handleFilter();
  }, false)
})
