---
background: Maskmtl_header.jpg
call_to_action: See the Catalog
featured:
- Pumpkin Embroidery Piece
- Bird Detachable Collar
- Flower Embroidery Bow
- Custom Embroidery
- Lily of the Valley
- Flower Embroidery Round Collar
---
## FAQ

{{% row %}}
{{% column %}}
### Types of Masks

All masks are available in two types. Folder: flexible and comfortable (rectangular).  
Regular: Shaped around the nose and the jaw. 

![Mask Types](masktype.png)
{{% /column %}}

{{% column %}}
### Pickups Only

Montreal only. No shipping, no deliveries, no fees. Curbside style pickups only available in 
two locations: Ville Saint-Laurent, Old-Port and Laval. Address addressed on order.
{{% /column %}}

{{% column %}}
### Sizing

Update: We no longer accept exchange due to safety measures. Once the masks are purchased, it will be non-refundable. Please choose your size carefully.  

Type Regular Size: 
Small : Fits most women 
Medium : Fits most men 
Large: upon request.
Kids size also available upon request.


Type Folder: One- Size fits all

{{% /column %}}
{{% /row %}}
